import * as assets from "@eternalfest/efc/java/assets";
import * as java from "@eternalfest/efc/java/level";
import * as hf from "@eternalfest/efc/compiler/hf";
import { parse as parseMtbon } from "@eternalfest/mtbon";

import fs from "fs/promises";
import path from "path";

function getCliArgs(): string[] {
  return process.argv.slice(2); // 2 and not 1, because of ts-node.
}

async function main() {
  const cliArgs = getCliArgs();
  if (cliArgs.length !== 2) {
    throw new Error("expected exactly 2 arguments");
  }
  const [inputFile, outputDir] = cliArgs;
  console.error(`Extracting levels ${inputFile} to ${outputDir}...`);

  const rawInput = await fs.readFile(inputFile, { encoding: "utf-8" });
  const javaAssets = (await assets.fromDirectories(assets.BASE_ASSETS_DIR)).unwrap();
  const mappings = computeAssetsMappings(javaAssets);

  const levels = rawInput.split(":").map((lvl, i) => {
    const javaLevel = rawToJavaLevel(lvl, mappings, "Niveau " + i);
    return javaLevelToXml(javaLevel);
  });

  const nbDigits = ("" + levels.length).length;
  await fs.mkdir(outputDir, { recursive: true });
  await Promise.all(levels.map((level, i) => {
      const digits = ("" + i).padStart(nbDigits, "0");
      const outFile = path.join(outputDir, `level${digits}.lvl`);
      return fs.writeFile(outFile, level, { encoding: "utf-8" });
  }));
}

interface AssetsMappings {
  readonly backgrounds: ReadonlyMap<number, string>;
  readonly bads: ReadonlyMap<number, string>;
  readonly fields: ReadonlyMap<number, string>;
  readonly tiles: ReadonlyMap<number, string>;
}

function computeAssetsMappings(raw: assets.JavaAssets): AssetsMappings {
  function reverse(raw: ReadonlyMap<string, assets.JavaAssetRef>): Map<number, string> {
    const map = new Map<number, string>();

    for (let asset of raw.values()) {
      const { value, id } = asset.ref;
      if (value === undefined) {
        continue;
      }

      const existing = map.get(value);
      if (existing !== undefined && existing != id) {
        throw new Error(`Conflicting assets: ${existing}, ${id} (for ${value})`);
      }

      map.set(value, id);
    }

    return map;
  }

  return {
    backgrounds: reverse(raw.backgrounds),
    bads: reverse(raw.bads),
    fields: reverse(raw.fields),
    tiles: reverse(raw.tiles),
  };
}



function rawToJavaLevel(raw: string, mappings: AssetsMappings, name: string): java.JavaLevel {
  const lvl = parseMtbon(raw) as hf.EscapedLevel;

  function valueToId(kind: keyof AssetsMappings, value: number): string {
    const id = mappings[kind].get(value);
    if (id === undefined) {
      throw new Error(`Unknown value ${value} in ${kind}`);
    }
    return id;
  }

  let script: java.Script;
  if (lvl.$script === null || lvl.$script === undefined) {
    script = { type: "xml", text: "" };
  } else if (typeof lvl.$script === "string") {
    script = { type: "xml", text: lvl.$script.replaceAll(/\r\n?/g, "\n") }; // normalize newlines
  } else {
    throw new Error(`Unsupported script in ${name}: ${lvl.$script}`);
  }

  const seenFields = new Map<string, string>();

  return {
    version: "1.2",
    name,
    description: "Extrait du jeu officiel",
    script,
    playerSlot: { x: lvl.$playerX, y: lvl.$playerY },
    skins: {
      background: valueToId("backgrounds", lvl.$skinBg),
      horizontalTiles: valueToId("tiles", lvl.$skinTiles % 100),
      verticalTiles: valueToId("tiles", lvl.$skinTiles < 100 ? lvl.$skinTiles : Math.floor(lvl.$skinTiles / 100)),
    },
    specialSlots: lvl.$specialSlots.map(slot => ({ x: slot.$x, y: slot.$y })),
    scoreSlots: lvl.$scoreSlots.map(slot => ({ x: slot.$x, y: slot.$y })),
    bads: lvl.$badList.map(bad => ({
      x: bad.$x,
      y: bad.$y,
      id: valueToId("bads", bad.$id),
    })),
    map: lvl.$map.map(col => col.map(raw => {
      if (raw === 0) {
        return "0";
      } else if (raw > 0) {
        return "1";
      } else {
        const fieldName = valueToId("fields", -raw);
        let field = seenFields.get(fieldName);
        if (field === undefined) {
          field = String.fromCharCode(0x41 /* A */ + seenFields.size);
          seenFields.set(fieldName, field);
        }
        return field;
      }
    })),
    fields: [...seenFields.keys()],
  };
}

function javaLevelToXml(lvl: java.JavaLevel): string {
  function posToAttrs(pos: { x: number, y: number }): string {
    return `x="${pos.x.toFixed(2)}" y="${pos.y.toFixed(2)}"`;
  }

  function scriptKind(script: java.Script): string {
    switch (script.type) {
      case "xml": return "xml";
      case "better": return "new";
    }
  }

  function spriteFrameForId(id: string): string {
    switch (id) {
      case "bad_spear": return "pic1";
      case "bad_litchi_weak": return "nu1";
      default: return "normal1";
    }
  }

  // We don't properly deal with escapes here :shrug:
  const lines = [
    `<?xml version="1.0" encoding="UTF-8"?>`,
    `<level version="1.2">`,
    `  <name>${lvl.name}</name>`,
    `  <desc>${lvl.description}</desc>`,
    `  <script type="${scriptKind(lvl.script)}"><![CDATA[${lvl.script.text}]]></script>`,
    `  <prefs draw="31" />`,
    `  <skins>`,
    `    <fond id="${lvl.skins.background}" />`,
    `    <plateformeH id="${lvl.skins.horizontalTiles}" />`,
    `    <plateformeV id="${lvl.skins.verticalTiles}" />`,
    `  </skins>`,
  ];

  if (lvl.fields.length > 0) {
    lines.push('  <rayons>');
    for (const f of lvl.fields) {
      lines.push(`    <r name="${f}"/>`);
    }
    lines.push('  </rayons>');
  }

  lines.push(`  <plateformes>${lvl.map.map(col => col.join("")).join("")}</plateformes>`);

  lines.push(
    `  <player id="spawn_igor" i="spawn_igor:base" ${posToAttrs(lvl.playerSlot)} f="0" r="0" />`
  );

  for (const slot of lvl.specialSlots) {
    lines.push(`  <effet id="spawn_obj_effet" i="spawn_obj_effet:base" ${posToAttrs(slot)} f="0" r="0" />`);
  }
  for (const slot of lvl.scoreSlots) {
    lines.push(`  <points id="spawn_obj_points" i="spawn_obj_points:base" ${posToAttrs(slot)} f="0" r="0" />`);
  }

  if (lvl.bads.length > 0) {
    lines.push('  <badlist>');
    for (const bad of lvl.bads) {
      lines.push(`    <bad id="${bad.id}" i="${bad.id}:${spriteFrameForId(bad.id)}" ${posToAttrs(bad)} f="0" r="0" />`);
    }
    lines.push('  </badlist>');
  }
  lines.push(`</level>\n`);

  return lines.join("\n");
}

main();
