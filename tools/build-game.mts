import { getGamePath } from "@eternalfest/game";
import { emitSwf } from "swf-emitter";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Movie, TagType } from "swf-types";
import { ImageType } from "swf-types/image-type.js";

import fs from "fs/promises";
import path from "path";
import { fileURLToPath } from 'url';

const PROJECT_ROOT: string = path.dirname(path.dirname(fileURLToPath(import.meta.url)));
const ASSETS_ROOT: string = path.join(PROJECT_ROOT, "src", "assets");

const INPUT_GAME: string = getGamePath();
const INPUT_ASSETS: string[] = [
    path.join(ASSETS_ROOT, "misc"),
    path.join(ASSETS_ROOT, "tiles"),
    path.join(ASSETS_ROOT, "backgrounds"),
];
const OUTPUT_GAME: string = path.join(PROJECT_ROOT, "build", "game.swf");

async function main() {
    const game = parseSwf(await fs.readFile(INPUT_GAME));

    const assets = await loadAssets(INPUT_ASSETS);

    replaceAssets(game, assets);
    const output = emitSwf(game, CompressionMethod.Deflate);
    await fs.writeFile(OUTPUT_GAME, output);
}

interface AssetData {
    id: number;
    mediaType: ImageType;
    bytes: Uint8Array;
}

async function loadAssets(dirs: string[]): Promise<AssetData[]> {


    async function load(dir: string): Promise<AssetData[]> {
        const files = await fs.readdir(dir);
        return Promise.all(files.map(async file => {
            const id = parseInt(file.split("-")[0].trim());
            if (isNaN(id) || id < 0) {
                throw new Error("Invalid asset file name: " + file);
            }

            var mediaType: ImageType;
            switch (path.extname(file)) {
                case ".png":
                    mediaType = "image/png";
                    break;
                case ".jpg":
                    mediaType = "image/jpeg";
                    break;
                default:
                    throw new Error("Unknown extension for asset: " + file);
            }

            const bytes = await fs.readFile(path.join(dir, file));
            return { id, mediaType, bytes };
        }));
    }

    return (await Promise.all(dirs.map(load))).flat();
}

function replaceAssets(game: Movie, assets: AssetData[]) {
    const map = new Map(assets.map(a => [a.id, a]));

    for (let i = 0; i < game.tags.length; i++) {
        const tag = game.tags[i];
        if (tag.type !== TagType.DefineBitmap) {
            continue;
        }

        const asset = map.get(tag.id);
        map.delete(tag.id);
        if (asset === undefined) {
            continue;
        }

        game.tags[i] = {
            type: TagType.DefineBitmap,
            id:  asset.id,
            mediaType: asset.mediaType,
            data: asset.bytes,
            // Assume the dimensions of the replaced asset match.
            // FIXME: this should be checked.
            width: tag.width,
            height: tag.height,
        };
    }

    if (map.size > 0) {
        throw new Error("Unknown asset IDs: " + [...map.keys()].join(", "));
    }
}

await main();
