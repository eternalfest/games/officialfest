# Hammerfest : Les Dimensions Parallèles

The official Hammerfest game, by Motion-Twin.

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/officialfest.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/officialfest.git
```

## High-quality assets

Optionally, compressed assets can be replaced by better quality versions obtained from [Motion-Twin's original sources](https://github.com/motion-twin/hammerfest).

Some compressed assets are left as-is:
- `523` (part of `$wheels1`): used, but never actually visible on screen;
- `605` (portal texture): missing in MT's files;
- `4656` (alternate score bar): unused in game.

Other compressed assets have been denoised with Waifu2x:
- `3881` ("Igor" map decoration).
